def bienbenido_at_monstapalace()
  puts "Well, you have made it to the house."
  puts "There is a large double door at the front of the house."
  puts "There is also a small side door that looks to lead to a small annex of the house."
  puts "Will you use the front door or the side entrance?"
  while true
    prompt()
    next_move = gets.chomp()
    if next_move == "front door"
      puts "The entry way give way. You plugs into a pit trap full of water."
      return :dead
    elsif next_move == "side entrance"
      puts "Good choice, You may continue."
      return :side_entrance
    else
      puts "I don\'t know which way you want to go."
    end
  end
end
